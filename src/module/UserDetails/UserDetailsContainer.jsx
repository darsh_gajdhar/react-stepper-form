import React from "react";
import UserDetails from "./UserDetails";

const UserDetailsContainer = ({ register, errors, control }) => (
  <UserDetails register={register} errors={errors} control={control} />
);
export default UserDetailsContainer;
