import React from "react";
import FormInputFieldText, { FormTitle } from "./style";

const FormInput = ({ fieldData, errors, register, title, control }) => (
  <>
    <FormTitle variant="h3">{title}</FormTitle>
    <FormInputFieldText
      name={fieldData.name}
      placeholder={fieldData.name}
      type={fieldData.type}
      {...register}
      {...control}
    />
    <span>{errors}</span>
  </>
);
export default FormInput;
