import React from "react";
import AccountCreation from "./AccountCreation";

const AccountCreationContainer = ({ register, errors, control }) => (
  <AccountCreation register={register} errors={errors} control={control} />
);

export default AccountCreationContainer;
