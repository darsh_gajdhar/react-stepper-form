import FormInput from "../../common/FormInputs/FormInputs";

const UserDetails = ({ register, errors, title, control }) => (
  <>
    <FormInput
      title={title}
      register={register("firstName")}
      fieldData={{
        name: "First Name",
        type: "text",
      }}
      errors={errors.firstName?.message}
      control={control}
    />
    <FormInput
      register={register("lastName")}
      fieldData={{
        name: "Last Name",
        type: "text",
      }}
      errors={errors.lastName?.message}
      control={control}
    />
    <FormInput
      register={register("age")}
      fieldData={{
        name: "Age",
        type: "number",
      }}
      errors={errors.age?.message}
      control={control}
    />
  </>
);

export default UserDetails;
