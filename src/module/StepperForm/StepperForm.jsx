import { Stepper, Step, StepLabel, Box } from "@mui/material";
import React from "react";
import FormComponent, {
  FormInputComponent,
  FormInputDesign,
  FormButton,
} from "./style";
import UserDetailsContainer from "../UserDetails/UserDetailsContainer";
import AccountCreationContainer from "../AccountCreation/AccountCreationContainer";
import AddressFormContainer from "../AddressForm/AddressFormContainer";

const StepperForm = ({
  onSubmit,
  handleSubmit,
  isFirstStep,
  isLastStep,
  back,
  steps,
  methods,
  currentStepIndex,
}) => {
  const {
    register,
    formState: { errors },
    control,
  } = methods;

  const getStepComponent = (step) => {
    switch (step) {
      case 0:
        return (
          <UserDetailsContainer
            register={register}
            errors={errors}
            title="User Details"
            control={control}
          />
        );
      case 1:
        return (
          <AddressFormContainer
            register={register}
            errors={errors}
            title="Address Form"
            control={control}
          />
        );
      case 2:
        return (
          <AccountCreationContainer
            register={register}
            errors={errors}
            title="Account Creation"
            control={control}
          />
        );
      default:
        return "Unknown Step";
    }
  };

  return (
    <FormComponent>
      <FormInputComponent>
        <FormInputDesign>
          <Stepper activeStep={currentStepIndex}>
            {steps.map((label) => {
              return (
                <Step key={label}>
                  <StepLabel
                    sx={{
                      span: { color: "rgb(141,30,141)" },
                      "svg > path": { color: "rgb(141,30,141)" },
                      svg: { color: "rgb(141,30,141)" },
                    }}
                  >
                    {label}
                  </StepLabel>
                </Step>
              );
            })}
          </Stepper>
          {getStepComponent(currentStepIndex)}
          <Box>
            {!isFirstStep && (
              <FormButton type="button" onClick={back}>
                Back
              </FormButton>
            )}
            <FormButton type="submit" onClick={handleSubmit(onSubmit)}>
              {isLastStep ? "Finish" : "Next"}
            </FormButton>
          </Box>
        </FormInputDesign>
      </FormInputComponent>
    </FormComponent>
  );
};

export default StepperForm;
