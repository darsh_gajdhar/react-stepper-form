import React from "react";
import AddressForm from "./AddressForm";

const AddressFormContainer = ({ register, errors, control }) => (
  <AddressForm register={register} errors={errors} control={control} />
);

export default AddressFormContainer;
