import * as yup from "yup";

export const validationSchema = [
  yup.object({
    firstName: yup.string().required("Please provide your First Name!"),

    lastName: yup.string().required("Please provide your Last Name!"),

    age: yup
      .number()
      .positive("Age should be not be negative!")
      .integer()
      .min(18, "Entered Age should be greater than 18")
      .max(65, "Entered Age should be greater than 65")
      .required("Please Enter Age!"),
  }),
  yup.object({
    street: yup.string().required("Please provide your Street Name!"),

    city: yup.string().required("Please provide your City Name!"),

    state: yup.string().required("Please provide your State Name!"),

    zipCode: yup
      .number()
      .min(100000, "ZipCode should be of 5 number")
      .max(999999, "ZipCode should be of 5 number")
      .required("Please provide your ZipCode"),
  }),

  yup.object({
    email: yup
      .string()
      .required("Please provide Email Address!")
      .email("Please provide valid email address!"),

    password: yup.string().min(4).max(20).required(),
  }),
];
