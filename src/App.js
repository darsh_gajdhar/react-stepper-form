import "./App.css";
import StepperFormContainer from "./module/StepperForm/StepperFormContainer";

function App() {
  return <StepperFormContainer />;
}

export default App;
