import React from "react";
import FormInput from "../../common/FormInputs/FormInputs";

const AddressForm = ({ register, errors, title, control }) => (
  <>
    <FormInput
      title={title}
      register={register("street")}
      fieldData={{
        name: "Street",
        type: "text",
      }}
      control={control}
      errors={errors.street?.message}
    />

    <FormInput
      register={register("city")}
      fieldData={{
        name: "City",
        type: "text",
      }}
      control={control}
      errors={errors.city?.message}
    />

    <FormInput
      register={register("state")}
      fieldData={{
        name: "State",
        type: "text",
      }}
      control={control}
      errors={errors.state?.message}
    />

    <FormInput
      register={register("zipCode")}
      fieldData={{
        name: "ZipCode",
        type: "number",
      }}
      control={control}
      errors={errors.zipCode?.message}
    />
  </>
);

export default AddressForm;
