import { Box, Button, Container } from "@mui/material";
import { styled } from "@mui/system";

const FormComponent = styled(Box, {
  name: "FormComponent",
})({
  backgroundColor: "#212121",
  height: "100vh",
  color: "whitesmoke",
  padding: "60px 0px",
});

export const FormInputComponent = styled(Box, {
  name: "FormInputComponent",
})({
  display: "flex",
  boxSizing: "border-box",
  justifyContent: "center",
  alignItems: "center",
  height: "auto",
});

export const FormInputDesign = styled(Container, {
  name: "FormInputDesign",
})({
  width: "50%",
  height: "auto",
  textAlign: "center",
  backgroundImage: "linear-gradient(to top, #a18cd1 40%, #fbc2eb 100%)",
  border: "1px solid rgb(141,30, 141)",
  borderRadius: "20px",
  display: "flex",
  padding: "40px 20px",
  justifyContent: "center",
  flexDirection: "column",
  "@media (min-width:0px) and (max-width:768px)": {
    width: "90%",
  },
  span: {
    color: "red",
    margin: "10px 0px",
    display: "block",
  },
});

export const FormButton = styled(Button, {
  name: "FormButton",
})({
  padding: "10px",
  margin: "35px 20px",
  width: "25%",
  height: "50px",
  backgroundColor: "rgb(141,30,141)",
  borderRadius: "12px",
  opacity: "0.7",
  color: "whitesmoke",
  trasition: "width 1.7s",
  fontSize: "1.13rem",
  fontWeight: "600",
  ":hover": {
    opacity: "1",
    width: "30%",
    backgroundColor: "rgb(141,30,141)",
  },
});

export default FormComponent;
