import React from "react";
import FormInput from "../../common/FormInputs/FormInputs";

const AccountCreation = ({ register, errors, title, control }) => (
  <>
    <FormInput
      title={title}
      register={register("email")}
      fieldData={{
        name: "Email",
        type: "email",
      }}
      control={control}
      errors={errors.email?.message}
    />

    <FormInput
      register={register("password")}
      fieldData={{
        name: "Password",
        type: "password",
      }}
      control={control}
      errors={errors.password?.message}
    />
  </>
);

export default AccountCreation;
