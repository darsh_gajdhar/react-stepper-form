import React, { useState } from "react";
import StepperForm from "./StepperForm";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validationSchema } from "./ValidationSchema";

const steps = ["User Details", "Address Form", "Account Creation"];

const StepperFormContainer = () => {
  const [currentStepIndex, setCurrentStepIndex] = useState(0);
  const isFirstStep = currentStepIndex === 0;
  const isLastStep = currentStepIndex === steps.length - 1;
  const currentValidationSchema = validationSchema[currentStepIndex];

  const methods = useForm({
    resolver: yupResolver(currentValidationSchema),
    mode: "onSubmit",
  });

  const { trigger, handleSubmit } = methods;

  const next = async () => {
    const isValidStep = await methods.trigger();
    if (isValidStep) setCurrentStepIndex((prevStepIndex) => prevStepIndex + 1);
  };

  const back = () => {
    setCurrentStepIndex((prevStepIndex) => prevStepIndex - 1);
  };

  const onSubmit = (data) => {
    if (!isLastStep) {
      trigger();
      return next();
    }

    alert(`${JSON.stringify(data)}`);
    console.log(data);
  };

  return (
    <FormProvider {...methods}>
      <StepperForm
        onSubmit={onSubmit}
        steps={steps}
        methods={methods}
        currentStepIndex={currentStepIndex}
        handleSubmit={handleSubmit}
        isFirstStep={isFirstStep}
        isLastStep={isLastStep}
        back={back}
      />
    </FormProvider>
  );
};

export default StepperFormContainer;
